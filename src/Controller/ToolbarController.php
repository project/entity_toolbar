<?php

namespace Drupal\entity_toolbar\Controller;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Template\Attribute;
use Drupal\entity_toolbar\Entity\EntityToolbarConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Render\RendererInterface;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ToolbarController.
 *
 * @package Drupal\admin_toolbar_tools\Controller
 */
class ToolbarController extends ControllerBase {

  use StringTranslationTrait;

  const BUNDLE_OFFSET = 10;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * The cache context manager service.
   *
   * @var \Drupal\Core\Cache\Context\CacheContextsManager
   */
  protected $cacheContextManager;

  /**
   * The toolbar cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $toolbarCache;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RendererInterface $renderer, ModuleHandlerInterface $module_handler, RouteProviderInterface $route_provider, CacheContextsManager $cache_context_manager, CacheBackendInterface $toolbar_cache) {
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
    $this->moduleHandler = $module_handler;
    $this->routeProvider = $route_provider;
    $this->cacheContextManager = $cache_context_manager;
    $this->toolbarCache = $toolbar_cache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('module_handler'),
      $container->get('router.route_provider'),
      $container->get('cache_contexts_manager'),
      $container->get('cache.toolbar')
    );
  }

  /**
   * Return entity toolbar as ajax.
   *
   * @param \Drupal\entity_toolbar\Entity\EntityToolbarConfig $toolbar
   *   Entity Toolbar config entity.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Ajax response.
   */
  public function lazyLoad(EntityToolbarConfig $toolbar) {

    // Using cache_toolbar rather than cache_render, since
    // developers usually turn off the render cache while developing.
    $additional_keys = $this->cacheContextManager->convertTokensToKeys([
      'languages:' . LanguageInterface::TYPE_INTERFACE,
      'user.permissions',
    ])->getKeys();
    $cid_parts = array_merge(['entity_toolbar:' . $toolbar->id() . '.data'], $additional_keys);
    $cid = implode(':', $cid_parts);

    if ($cache = $this->toolbarCache->get($cid)) {
      $build = $cache->data;
    }
    else {
      $build = $this->buildToolbar($toolbar);
      $cache_tags = $build['toolbar']['#cache']['tags'];
      $build = $this->renderer->renderPlain($build);
      $this->toolbarCache
        ->set($cid, $build, Cache::PERMANENT, $cache_tags);
    }

    $response = (new Response($build))
      // Do not allow any intermediary to cache the response, only the end user.
      ->setPrivate()
      // Allow the end user to cache it for up to 5 minutes.
      ->setMaxAge(300);

    return $response;
  }

  /**
   * @param $toolbar
   */
  protected function buildToolbar(EntityToolbarConfig $toolbar) {

    $items = [];
    $cache_tags = $toolbar->getCacheTagsToInvalidate();

    $content_entity_bundle = $toolbar->getBundleEntityId();
    $bundles = $content_entity_bundle_storage = $this->entityTypeManager
      ->getStorage($content_entity_bundle)
      ->loadMultiple();
    uasort($bundles, [static::class, 'sort']);

    foreach ($bundles as $machine_name => $bundle) {

      // Remove punctuation before alphabetizing.
      $link_title = str_replace(['"', "'"], "", $bundle->label());
      $group_name = ucwords(substr($link_title, 0, 1));

      $url = Url::fromRoute('entity.node_type.collection');
      $url->setOption('attributes', [
        'class' => [
          'admin-toolbar-search-ignore',
        ],
      ]);

      if (empty($items['entity.node_type.collection.' . $group_name])) {
        $items['entity.node_type.collection.' . $group_name] = [
          'is_expanded' => TRUE,
          'is_collapsed' => TRUE,
          // We don't ever want to set active class on the clones of the
          // collection page used for each letter, or else all of them
          // will be highlit on the collection page.
          'in_active_trail' => FALSE,
          'attributes' => new Attribute([]),
          'title' => $group_name,
          'url' => $url,
          'below' => [],
          'original_link' => NULL,
        ];
      }

      $content_entity = $bundle->getEntityType()->get('bundle_of');
      $cache_tags = Cache::mergeTags($cache_tags, $bundle->getEntityType()->getListCacheTags());
      $params = [
        $content_entity_bundle => $machine_name,
      ];
      if ($this->routeExists('entity.' . $content_entity_bundle . '.overview_form')) {
        // Some bundles have an overview/list form that make a better root
        // link.
        $url = Url::fromRoute('entity.' . $content_entity_bundle . '.overview_form', $params);
        $items['entity.node_type.collection.' . $group_name]['below'][$bundle->label()] = [
          'is_expanded' => TRUE,
          'is_collapsed' => FALSE,
          'in_active_trail' => FALSE,
          'attributes' => new Attribute([]),
          'title' => $bundle->label(),
          'url' => $url,
          'below' => [],
          'original_link' => NULL,
        ];

      }
      elseif ($this->routeExists('entity.' . $content_entity_bundle . '.edit_form')) {
        $url = Url::fromRoute('entity.' . $content_entity_bundle . '.edit_form', $params);
        $items['entity.node_type.collection.' . $group_name]['below'][$bundle->label()] = [
          'is_expanded' => TRUE,
          'is_collapsed' => FALSE,
          'in_active_trail' => FALSE,
          'attributes' => new Attribute([]),
          'title' => $bundle->label(),
          'url' => $url,
          'below' => [],
          'original_link' => NULL,
        ];

      }

      if ($this->moduleHandler->moduleExists('field_ui')) {
        if ($this->routeExists('entity.' . $content_entity . '.field_ui_fields')) {
          $url = Url::fromRoute('entity.' . $content_entity . '.field_ui_fields', $params);
          $items['entity.node_type.collection.' . $group_name]['below'][$bundle->label()]['below'][] = [
            'is_expanded' => FALSE,
            'is_collapsed' => FALSE,
            'in_active_trail' => FALSE,
            'attributes' => new Attribute([]),
            'title' => $this->t('Manage fields'),
            'url' => $url,
            'below' => [],
            'original_link' => NULL,
          ];
        }

        if ($this->routeExists('entity.entity_form_display.' . $content_entity . '.default')) {
          $url = Url::fromRoute('entity.entity_form_display.' . $content_entity . '.default', $params);
          $items['entity.node_type.collection.' . $group_name]['below'][$bundle->label()]['below'][] = [
            'is_expanded' => FALSE,
            'is_collapsed' => FALSE,
            'in_active_trail' => FALSE,
            'attributes' => new Attribute([]),
            'title' => $this->t('Manage form display'),
            'url' => $url,
            'below' => [],
            'original_link' => NULL,
          ];

        }
        if ($this->routeExists('entity.entity_view_display.' . $content_entity . '.default')) {
          $url = Url::fromRoute('entity.entity_view_display.' . $content_entity . '.default', $params);
          $items['entity.node_type.collection.' . $group_name]['below'][$bundle->label()]['below'][] = [
            'is_expanded' => FALSE,
            'is_collapsed' => FALSE,
            'in_active_trail' => FALSE,
            'attributes' => new Attribute([]),
            'title' => $this->t('Manage display'),
            'url' => $url,
            'below' => [],
            'original_link' => NULL,
          ];
        }
        if ($this->moduleHandler->moduleExists('devel') && $this->routeExists('entity.' . $content_entity_bundle . '.devel_load')) {
          $url = Url::fromRoute($route_name = 'entity.' . $content_entity_bundle . '.devel_load', $params);
          $items['entity.node_type.collection.' . $group_name]['below'][$bundle->label()]['below'][] = [
            'is_expanded' => FALSE,
            'is_collapsed' => FALSE,
            'in_active_trail' => FALSE,
            'attributes' => new Attribute([]),
            'title' => $this->t('Devel'),
            'url' => $url,
            'below' => [],
            'original_link' => NULL,
          ];
        }
        if ($this->routeExists('entity.' . $content_entity_bundle . '.delete_form')) {
          $url = Url::fromRoute('entity.' . $content_entity_bundle . '.delete_form', $params);
          $items['entity.node_type.collection.' . $group_name]['below'][$bundle->label()]['below'][] = [
            'is_expanded' => FALSE,
            'is_collapsed' => FALSE,
            'in_active_trail' => FALSE,
            'attributes' => new Attribute([]),
            'title' => $this->t('Delete'),
            'url' => $url,
            'below' => [],
            'original_link' => NULL,
          ];
        }
      }
    }

    if (empty($toolbar->get('noGroup'))) {
      foreach ($items as $key => $link) {
        if (count($link['below']) > 8) {
          $count = count($link['below']);
          $devisor = 8;
          while ($count % $devisor < 4 && $devisor > 5) {
            $devisor--;
          }
          $chunks = array_chunk($link['below'], $devisor, TRUE);
          $items[$key]['below'] = array_shift($chunks);
          foreach ($chunks as $chunk_key => $chunk) {
            $items[$key . '-' . $chunk_key] = $link;
            $items[$key . '-' . $chunk_key]['below'] = $chunk;
          }
        }
      }
    }
    else {
      $new_items = [];
      foreach ($items as $key => $link) {
        $new_items = array_merge($new_items, $link['below']);
      }
      $items = $new_items;
    }

    ksort($items);

    if ($add_route_name = $toolbar->get('addRouteName')) {

      $link_text = $toolbar->get('addRouteLinkText');

      if (empty($link_text)) {
        $entity_type = $this->entityTypeManager->getDefinition($toolbar->id());
        $link_text = new TranslatableMarkup('Add @type type', ['@type' => $entity_type->getLabel()]);
      }

      $items[] = [
        'is_expanded' => FALSE,
        'is_collapsed' => TRUE,
        'in_active_trail' => FALSE,
        'attributes' => new Attribute([]),
        'title' => $link_text,
        'url' => Url::fromRoute($add_route_name),
        'below' => [],
        'original_link' => NULL,
      ];
    }

    return [
      'toolbar' => [
        '#theme' => 'menu__toolbar__admin',
        '#items' => $items,
        '#attributes' => ['class' => 'clearfix'],
        '#menu_name' => 'entity-toolbar',
        '#cache' => [
          'tags' => $cache_tags,
        ],
      ],
    ];

  }

  /**
   * Get a list of content entities.
   *
   * @return array
   *   An array of metadata about content entities.
   */
  protected function getBundleableEntitiesList() {
    $entity_types = $this->entityTypeManager->getDefinitions();
    $content_entities = [];
    foreach ($entity_types as $key => $entity_type) {
      if ($entity_type->getBundleEntityType() && ($entity_type->get('field_ui_base_route') != '')) {
        $content_entities[$key] = [
          'content_entity' => $key,
          'content_entity_bundle' => $entity_type->getBundleEntityType(),
        ];
      }
    }
    return $content_entities;
  }

  /**
   * Determine if a route exists by name.
   *
   * @param string $route_name
   *   The name of the route to check.
   *
   * @return bool
   *   Whether a route with that route name exists.
   */
  public function routeExists($route_name) {
    return (count($this->routeProvider->getRoutesByNames([$route_name])) === 1);
  }

  /**
   * Helper callback for uasort() to sort configuration entities by weight and label.
   */
  public static function sort(ConfigEntityInterface $a, ConfigEntityInterface $b) {
    $a_weight = isset($a->weight) ? $a->weight : 0;
    $b_weight = isset($b->weight) ? $b->weight : 0;
    if ($a_weight == $b_weight) {
      $a_label = str_replace(['"', "'"], "", $a->label());
      $b_label = str_replace(['"', "'"], "", $b->label());
      return strnatcasecmp($a_label, $b_label);
    }
    return ($a_weight < $b_weight) ? -1 : 1;
  }

}
