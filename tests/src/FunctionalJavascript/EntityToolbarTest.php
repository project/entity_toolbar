<?php

namespace Drupal\Tests\entity_toolbar\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Test entity toolbar functionality.
 *
 * @group entity_toolbar
 */
class EntityToolbarTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'node',
    'taxonomy',
    'entity_toolbar',
  ];

  /**
   * The admin user for tests.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * The user for tests.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $toolbarUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    // Create Basic page and Article node types.
    if ($this->profile != 'standard') {
      $this->drupalCreateContentType([
        'type' => 'page',
        'name' => 'Basic page',
      ]);
      $this->drupalCreateContentType([
        'type' => 'article',
        'name' => 'Article',
      ]);
    }

    $vocabulary = Vocabulary::create([
      'name' => 'Llama',
      'vid' => 'llama',
    ]);
    $vocabulary->save();

    $vocabulary = Vocabulary::create([
      'name' => 'Zebra',
      'vid' => 'zebra',
    ]);
    $vocabulary->save();

    $this->adminUser = $this->drupalCreateUser([
      'access toolbar',
      'administer menu',
      'access administration pages',
      'administer site configuration',
      'administer taxonomy',
      'administer content types',
    ]);

    $this->toolbarUser = $this->drupalCreateUser([
      'access toolbar',
    ]);
  }

  /**
   * Tests toolbar integration.
   */
  public function testToolbarIntegration() {
    $library_css_url = 'entity_toolbar/css/entity.toolbar.css';
    $toolbar_selector = '#toolbar-bar .toolbar-tab';
    $taxonomy_tab = '#toolbar-item-taxonomy-term';
    $taxonomy_tray = '#toolbar-item-taxonomy-term-tray';

    $node_tab = '#toolbar-item-node';
    $node_tray = '#toolbar-item-node-tray';

    // Ensures that node types toolbar item is accessible only for user with the
    // adequate permissions.
    $this->drupalGet('');
    $this->assertSession()->responseNotContains($library_css_url);
    $this->assertSession()->elementNotExists('css', $toolbar_selector);
    $this->assertSession()->elementNotExists('css', $taxonomy_tab);

    $this->drupalLogin($this->toolbarUser);
    $this->assertSession()->responseNotContains($library_css_url);
    $this->assertSession()->elementExists('css', $toolbar_selector);
    $this->assertSession()->elementNotExists('css', $taxonomy_tab);
    $this->assertSession()->elementNotExists('css', $node_tab);

    $this->drupalLogin($this->adminUser);

    $this->assertSession()->elementExists('css', $toolbar_selector);

    $this->assertSession()->responseContains($library_css_url);
    $this->assertSession()->elementExists('css', $toolbar_selector);
    $this->assertSession()->elementExists('css', $taxonomy_tab);
    $this->assertSession()->elementExists('css', $node_tab);
    $this->assertSession()->elementTextContains('css', $taxonomy_tab, 'Term Types');
    $this->assertSession()->elementTextContains('css', $node_tab, 'Content Types');

    $this->assertSession()->elementExists('css', $taxonomy_tray);
    $this->assertSession()->elementExists('css', $node_tray);

  }

  /**
   * Tests toolbar config forms.
   */
  public function testToolbarConfigForms() {

    $this->drupalLogin($this->adminUser);

    // Test "node" config form.
    $this->drupalGet('/admin/config/content/entity_toolbar/node/edit');
    $field_values = [
      'label' => 'Content types',
      'addRouteName' => 'node.type_add',
      'addRouteLinkText' => 'Add Node type',
    ];
    foreach ($field_values as $field => $value) {
      $this->assertSession()->fieldValueEquals($field, $value);
    }
    $this->assertSession()
      ->fieldExists('label')
      ->setValue('Node types');
    $this->assertSession()->buttonExists('Save')->press();
    $this->assertSession()->pageTextContains('Saved the Node types Entity Toolbar.');

    // Test "taxonomy_term" config form.
    $this->drupalGet('/admin/config/content/entity_toolbar/taxonomy_term/edit');
    $field_values = [
      'label' => 'Term types',
      'addRouteName' => 'entity.taxonomy_vocabulary.add_form',
      'addRouteLinkText' => 'Add Term type',
    ];
    foreach ($field_values as $field => $value) {
      $this->assertSession()->fieldValueEquals($field, $value);
    }
    $this->assertSession()
      ->fieldExists('label')
      ->setValue('Vocabularies');
    $this->assertSession()->buttonExists('Save')->press();
    $this->assertSession()->pageTextContains('Saved the Vocabularies Entity Toolbar.');

    // Test ajax responses.
    $this->drupalGet('/admin/entity_toolbar/node');
    $expected_links = [
      '/admin/structure/types/manage/article',
      '/admin/structure/types/manage/article/fields',
      '/admin/structure/types/manage/article/form-display',
      '/admin/structure/types/manage/article/display',
      '/admin/structure/types/manage/article/delete',
      '/admin/structure/types/manage/page',
      '/admin/structure/types/manage/page/fields',
      '/admin/structure/types/manage/page/form-display',
      '/admin/structure/types/manage/page/display',
      '/admin/structure/types/manage/page/delete',
      '/admin/structure/types/add',
    ];
    foreach ($expected_links as $expected_link) {
      $this->assertSession()->linkByHrefExists($expected_link);
    }

    $this->drupalGet('/admin/entity_toolbar/taxonomy_term');
    $this->assertLetterGroupLink("L");
    $this->assertLetterGroupLink("Z");
    $this->assertTermLinks();
    $this->drupalGet('admin/config/content/entity_toolbar/taxonomy_term/edit');
    // Test disabling the letter grouping.
    $this->assertSession()->fieldExists('noGroup')->check();
    $this->assertSession()->buttonExists('Save')->press();
    $this->assertSession()->pageTextContains('Saved the Vocabularies Entity Toolbar.');
    $this->drupalGet('/admin/entity_toolbar/taxonomy_term');
    $this->getSession()->reload();
    $this->assertNoLetterGroupLink("L");
    $this->assertNoLetterGroupLink("Z");
    $this->assertTermLinks();
  }

  /**
   * Test that letter group exists.
   *
   * @param string $letter
   *   Letter of the alphabet in a menu link parent to bundles starting
   *   with that letter.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  protected function assertLetterGroupLink($letter) {
    $letter_link = $this->assertSession()
      ->elementExists('xpath', '//a[text()="' . $letter . '"]');
    $this->assertTrue($letter_link->hasClass('admin-toolbar-search-ignore'));
  }

  /**
   * Test that letter group does not exist.
   *
   * @param string $letter
   *   Letter of the alphabet in a menu link parent to bundles starting
   *   with that letter.
   */
  protected function assertNoLetterGroupLink($letter) {
    $this->assertSession()
      ->elementNotExists('xpath', '//a[text()="' . $letter . '"]');
  }

  /**
   * Test that llama and zebra links are present.
   */
  protected function assertTermLinks() {
    $expected_links = [
      '/admin/structure/taxonomy/manage/llama/overview',
      '/admin/structure/taxonomy/manage/llama/overview/fields',
      '/admin/structure/taxonomy/manage/llama/overview/form-display',
      '/admin/structure/taxonomy/manage/llama/overview/display',
      '/admin/structure/taxonomy/manage/llama/delete',
      '/admin/structure/taxonomy/manage/zebra/overview',
      '/admin/structure/taxonomy/manage/zebra/overview/fields',
      '/admin/structure/taxonomy/manage/zebra/overview/form-display',
      '/admin/structure/taxonomy/manage/zebra/overview/display',
      '/admin/structure/taxonomy/manage/zebra/delete',
      '/admin/structure/taxonomy/add',
    ];
    foreach ($expected_links as $expected_link) {
      $this->assertSession()
        ->elementExists('xpath', '//a[contains(@href, "' . $expected_link . '")]');
    }
  }

}
